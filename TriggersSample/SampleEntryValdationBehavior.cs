﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace TriggersSample
{
    public class SampleEntryValdationBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry entry)
        {
            entry.TextChanged += OnEntryTextChanged;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnEntryTextChanged;
            base.OnDetachingFrom(entry);
        }

        void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            bool containsNumbers = args.NewTextValue.Any(char.IsDigit);
            ((Entry)sender).TextColor = containsNumbers ? Color.Red : Color.Default;
        }
    }
}
